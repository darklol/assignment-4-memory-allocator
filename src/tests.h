#ifndef TEST_H
#define TEST_H

#include "mem.h"

void malloc_test(struct block_header *heap_start);

void free_one_block_test(struct block_header *heap_start);

void free_two_blocks_test(struct block_header *heap_start);

void grow_heap_test(struct block_header *heap_start);

void grow_heap_new_reg_alloc_test(struct block_header *heap_start);

void run_tests();

#endif
