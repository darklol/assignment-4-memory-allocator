#include "tests.h"
#include "mem.h"
#include "mem_internals.h"
#include "util.h"


#define HEAP_SIZE 42

void malloc_test(struct block_header *heap_start) {
    printf("Тест: выделения памяти\n");
    void *allocated = _malloc(42);
    if (allocated == NULL) {
        err("Тест провален: вернулось NULL\n");
    }
    debug_heap(stdout, heap_start);
    if (heap_start->capacity.bytes != 42)  {
        err("Тест провален: вернулось неверное значение\n");
    }
    if(heap_start->is_free != false){
        err("Тест провален: некорректное выделение блока\n");
    }
    printf("Тест пройден\n");
    printf("--------------------------\n");
    _free(allocated);
}

void free_one_block_test(struct block_header *heap_start) {
    printf("Тест: освобождение одного блока\n");
    void *allocated1 = _malloc(42);
    void *allocated2 = _malloc(84);
    if (allocated1 == NULL || allocated2 == NULL) {
        err("Тест провален: вернулось NULL\n");
    }
    _free(allocated1);
    debug_heap(stdout, heap_start);
    if (!allocated1 || allocated2) {
        err("Тест провален: некорректное выделение блока\n");
    }

    printf("Тест пройден\n");
    printf("--------------------------\n");
    _free(allocated2);
}

void free_two_blocks_test(struct block_header *heap_start) {
    printf("Тест: освобождение двух блоков\n");
    void *allocated1 = _malloc(42);
    void *allocated2 = _malloc(84);
    void *allocated3 = _malloc(126);
    void *allocated4 = _malloc(168);
    void *allocated5 = _malloc(210);
    if (allocated1 == NULL || allocated2 == NULL || allocated3 == NULL || allocated4 == NULL || allocated5 == NULL) {
        err("Тест провален: вернулось NULL\n");
    }
    _free(allocated3);
    debug_heap(stdout, heap_start);
    if (allocated3) {
        err("Тест провален: некорректное выделение блока\n");
    }
    _free(allocated4);
    debug_heap(stdout, heap_start);
    if (allocated4) {
        err("Тест провален: некорректное выделение блока\n");
    }
    printf("Тест пройден\n");
    printf("--------------------------\n");
    _free(allocated1);
    _free(allocated2);
    _free(allocated5);
}

void grow_heap_test(struct block_header *heap_start) {
    printf("Тест: расширение памяти\n");
    void *allocated1 = _malloc(42);
    void *allocated2 = _malloc(84);
    debug_heap(stdout, heap_start);
    struct block_header *header1 = block_get_header(allocated1);
    struct block_header *header2 = block_get_header(allocated2);
    if ((uint8_t *) header1->contents + header2->capacity.bytes != (uint8_t *) header2) {
        err("Тест провален: неверное расширение памяти\n");
    }
    printf("Тест пройден\n");
    printf("--------------------------\n");
    _free(allocated1);
    _free(allocated2);
}

void grow_heap_new_reg_alloc_test(struct block_header *heap_start) {
    printf("Тест: расширение памяти и релокация\n");
    struct block_header* heap_end = heap_start;
    debug_heap(stdout, heap_start);
    while(heap_end->next != NULL) {
        heap_end = heap_end->next;
    }
    debug_heap(stdout, heap_start);
    void* allocated = _malloc(424242);
    debug_heap(stdout, heap_start);
    struct block_header* block = block_get_header(allocated);
    if (block == heap_end) {
        err("Тест провален: неверное расширение памяти\n");
    }
    printf("Тест пройден\n");
    printf("--------------------------\n");
    _free(allocated);
}

void run_tests() {
    struct block_header *heap_start = (struct block_header *) heap_init(HEAP_SIZE);

    malloc_test(heap_start);

    free_one_block_test(heap_start);

    free_two_blocks_test(heap_start);

    grow_heap_test(heap_start);

    grow_heap_new_reg_alloc_test(heap_start);
}
